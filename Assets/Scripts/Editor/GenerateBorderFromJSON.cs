﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;


namespace InfinityCode.RealWorldTerrain
{
    public class GenerateBorderFromJSON : EditorWindow
    {
        private string filename;
        private RealWorldTerrainContainer container;

        private void GeneratePOI()
        {
            if (!ValidateFields()) return;

            OnlineMapsJSONObject json = OnlineMapsJSONObject.ParseObject(File.ReadAllText(filename));
            OnlineMapsJSONArray geometries = json["//geometry"] as OnlineMapsJSONArray;
            if (geometries == null || geometries.count == 0)
            {
                EditorUtility.DisplayDialog("Error", "Invalid file structure. This script require geometry section.", "OK");
            }

            int numFeatures = geometries.count;

            GameObject bGO = new GameObject("Borders");

            EditorUtility.DisplayCancelableProgressBar("Generate borders", "", 0);

            List<Vector3> positions = new List<Vector3>();

            for (int i = 0; i < numFeatures; i++)
            {
                OnlineMapsJSONItem geometry = geometries[i];
                string type = geometry["type"].Value<string>();
                OnlineMapsJSONItem coordinates = geometry["coordinates"];

                positions.Clear();

                if (type == "MultiLineString")
                {
                    foreach (OnlineMapsJSONItem ci in coordinates)
                    {
                        positions.Clear();

                        foreach (OnlineMapsJSONItem item in ci)
                        {
                            double lng = item[0].Value<double>();
                            double lat = item[1].Value<double>();
                            Vector3 pos;
                            RealWorldTerrainLookLngLat.GetRealWorldPoint(out pos, (float)lng, (float)lat);
                            positions.Add(pos);
                        }

                        CreateLineRenderer(i, bGO, positions);
                    }
                }
                else if (type == "LineString")
                {
                    foreach (OnlineMapsJSONItem ci in coordinates)
                    {
                        double lng = ci[0].Value<double>();
                        double lat = ci[1].Value<double>();
                        Vector3 pos;
                        RealWorldTerrainLookLngLat.GetRealWorldPoint(out pos, (float)lng, (float)lat);
                        positions.Add(pos);
                    }
                    CreateLineRenderer(i, bGO, positions);
                }
                else if (type == "MultiPolygon")
                {
                    foreach (OnlineMapsJSONItem ci in coordinates)
                    {
                        foreach (OnlineMapsJSONItem ci2 in ci)
                        {
                            positions.Clear();

                            foreach (OnlineMapsJSONItem ci3 in ci2)
                            {
                                double lng = ci3[0].Value<double>();
                                double lat = ci3[1].Value<double>();
                                Vector3 pos;
                                RealWorldTerrainLookLngLat.GetRealWorldPoint(out pos, (float)lng, (float)lat);
                                positions.Add(pos);
                            }

                            CreateLineRenderer(i, bGO, positions);
                        }
                    }
                }
                else if (type == "Polygon")
                {
                    foreach (OnlineMapsJSONItem ci in coordinates)
                    {
                        foreach (OnlineMapsJSONItem ci2 in ci)
                        { 
                             
                            double lng = ci2[0].Value<double>();
                            double lat = ci2[1].Value<double>();
                            Vector3 pos;
                            RealWorldTerrainLookLngLat.GetRealWorldPoint(out pos, (float)lng, (float)lat);
                            positions.Add(pos); 

                            CreateLineRenderer(i, bGO, positions);
                        }
                    }
                }

                else Debug.Log(type);


                if (EditorUtility.DisplayCancelableProgressBar("Generate borders", "", i / (float)numFeatures))
                {
                    return;
                }
            }

            EditorUtility.ClearProgressBar();
        }

        private static void CreateLineRenderer(int i, GameObject bGO, List<Vector3> positions)
        {
            positions.RemoveAll(p => p == Vector3.zero);

            if (positions.Count < 2) return;

            GameObject go = new GameObject("Feature " + i);
            go.transform.parent = bGO.transform;
            LineRenderer lr = go.AddComponent<LineRenderer>();
            lr.useWorldSpace = true;
            lr.SetVertexCount(positions.Count);
            lr.SetPositions(positions.ToArray());
        }

        private void OnGUI()
        {
            GUILayout.BeginHorizontal();
            GUILayout.Label("Filename: ", GUILayout.ExpandWidth(false));
            EditorGUI.BeginDisabledGroup(true);
            GUILayout.TextField(!string.IsNullOrEmpty(filename) ? filename : "");
            EditorGUI.EndDisabledGroup();
            if (GUILayout.Button("...", GUILayout.ExpandWidth(false))) filename = EditorUtility.OpenFilePanel("JSON file", Application.dataPath, "js");
            GUILayout.EndHorizontal();
            container = EditorGUILayout.ObjectField("RWT Container: ", container, typeof(RealWorldTerrainContainer), true) as RealWorldTerrainContainer;
            if (GUILayout.Button("Generate"))
            {
                try
                {
                    GeneratePOI();
                }
                catch (Exception)
                {
                    EditorUtility.ClearProgressBar();
                    throw;
                }
            }
        }

        [MenuItem("Window/Generate Borders from JSON")]
        private static void OpenWindow()
        {
            GenerateBorderFromJSON wnd = GetWindow<GenerateBorderFromJSON>(true, "Generate Borders from JSON");
            wnd.container = FindObjectOfType<RealWorldTerrainContainer>();
        }

        private bool ValidateFields()
        {
            if (string.IsNullOrEmpty(filename))
            {
                EditorUtility.DisplayDialog("Error", "Filename is empty.", "OK");
                return false;
            }

            if (container == null)
            {
                EditorUtility.DisplayDialog("Error", "RWT Container is null.", "OK");
                return false;
            }

            return true;
        }
    }
}