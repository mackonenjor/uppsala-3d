﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public abstract class OnlineMapsJSONItem : IEnumerable<OnlineMapsJSONItem>
{
    public abstract OnlineMapsJSONItem this[string key] { get; }

    public abstract OnlineMapsJSONItem this[int index] { get; }

    public T Deserialize<T>()
    {
        return (T)Deserialize(typeof(T));
    }

    public abstract object Deserialize(Type type);

    public abstract OnlineMapsJSONItem GetAll(string key);

    public abstract void ToJSON(StringBuilder b);

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public virtual IEnumerator<OnlineMapsJSONItem> GetEnumerator()
    {
        return null;
    }

    public override string ToString()
    {
        StringBuilder b = new StringBuilder();
        ToJSON(b);
        return b.ToString();
    }

    public abstract object Value(Type type);

    public virtual T Value<T>()
    {
        return default(T);
    }
}
